#!/bin/sh

convert IMG-20211003-WA0000.jpg -resize 320x200 ../thumbs/IMG-20211003-WA0000.jpg
convert IMG-20211003-WA0001.jpg -resize 320x200 ../thumbs/IMG-20211003-WA0001.jpg
convert IMG-20211003-WA0002.jpg -resize 320x200 ../thumbs/IMG-20211003-WA0002.jpg
convert IMG-20211003-WA0003.jpg -resize 320x200 ../thumbs/IMG-20211003-WA0003.jpg
convert IMG-20211003-WA0004.jpg -resize 320x200 ../thumbs/IMG-20211003-WA0004.jpg
convert IMG-20211125-WA0000.jpg -resize 320x200 ../thumbs/IMG-20211125-WA0000.jpg

convert vlcsnap-2021-10-03-16h38m27s731.png -resize 320x200 ../thumbs/vlcsnap-2021-10-03-16h38m27s731.png
convert vlcsnap-2021-10-03-16h40m37s683.png -resize 320x200 ../thumbs/vlcsnap-2021-10-03-16h40m37s683.png
convert vlcsnap-2021-10-03-16h41m22s396.png -resize 320x200 ../thumbs/vlcsnap-2021-10-03-16h41m22s396.png
convert vlcsnap-2021-10-03-16h42m06s739.png -resize 320x200 ../thumbs/vlcsnap-2021-10-03-16h42m06s739.png
convert vlcsnap-2021-10-03-16h42m58s174.png -resize 320x200 ../thumbs/vlcsnap-2021-10-03-16h42m58s174.png
